<?php

namespace Game\STOREBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Game\STOREBundle\Entity\Product_Type;
use Game\STOREBundle\Form\Product_TypeType;

/**
 * Product_Type controller.
 *
 */
class Product_TypeController extends Controller
{
    /**
     * Lists all Product_Type entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $product_Types = $em->getRepository('STOREBundle:Product_Type')->findAll();

        return $this->render('product_type/index.html.twig', array(
            'product_Types' => $product_Types,
        ));
    }

    /**
     * Creates a new Product_Type entity.
     *
     */
    public function newAction(Request $request)
    {
        $product_Type = new Product_Type();
        $form = $this->createForm('Game\STOREBundle\Form\Product_TypeType', $product_Type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product_Type);
            $em->flush();

            return $this->redirectToRoute('bunker_types_show', array('id' => $product_Type->getId()));
        }

        return $this->render('product_type/new.html.twig', array(
            'product_Type' => $product_Type,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Product_Type entity.
     *
     */
    public function showAction(Product_Type $product_Type)
    {
        $deleteForm = $this->createDeleteForm($product_Type);

        return $this->render('product_type/show.html.twig', array(
            'product_Type' => $product_Type,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Product_Type entity.
     *
     */
    public function editAction(Request $request, Product_Type $product_Type)
    {
        $deleteForm = $this->createDeleteForm($product_Type);
        $editForm = $this->createForm('Game\STOREBundle\Form\Product_TypeType', $product_Type);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product_Type);
            $em->flush();

            return $this->redirectToRoute('bunker_types_edit', array('id' => $product_Type->getId()));
        }

        return $this->render('product_type/edit.html.twig', array(
            'product_Type' => $product_Type,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Product_Type entity.
     *
     */
    public function deleteAction(Request $request, Product_Type $product_Type)
    {
        $form = $this->createDeleteForm($product_Type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product_Type);
            $em->flush();
        }

        return $this->redirectToRoute('bunker_types_index');
    }

    /**
     * Creates a form to delete a Product_Type entity.
     *
     * @param Product_Type $product_Type The Product_Type entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product_Type $product_Type)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bunker_types_delete', array('id' => $product_Type->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
