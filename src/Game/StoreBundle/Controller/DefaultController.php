<?php

namespace Game\STOREBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('STOREBundle:Default:index.html.twig');
    }
}
