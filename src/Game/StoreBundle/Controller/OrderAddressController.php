<?php

namespace Game\STOREBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Game\STOREBundle\Entity\OrderAddress;
use Game\STOREBundle\Form\OrderAddressType;

/**
 * OrderAddress controller.
 *
 */
class OrderAddressController extends Controller
{
    /**
     * Lists all OrderAddress entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $orderAddresses = $em->getRepository('STOREBundle:OrderAddress')->findAll();

        return $this->render('orderaddress/index.html.twig', array(
            'orderAddresses' => $orderAddresses,
        ));
    }

    /**
     * Creates a new OrderAddress entity.
     *
     */
    public function newAction(Request $request)
    {
        $orderAddress = new OrderAddress();
        $form = $this->createForm('Game\STOREBundle\Form\OrderAddressType', $orderAddress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($orderAddress);
            $em->flush();

            return $this->redirectToRoute('bunker_orderaddress_show', array('id' => $orderAddress->getId()));
        }

        return $this->render('orderaddress/new.html.twig', array(
            'orderAddress' => $orderAddress,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a OrderAddress entity.
     *
     */
    public function showAction(OrderAddress $orderAddress)
    {
        $deleteForm = $this->createDeleteForm($orderAddress);

        return $this->render('orderaddress/show.html.twig', array(
            'orderAddress' => $orderAddress,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing OrderAddress entity.
     *
     */
    public function editAction(Request $request, OrderAddress $orderAddress)
    {
        $deleteForm = $this->createDeleteForm($orderAddress);
        $editForm = $this->createForm('Game\STOREBundle\Form\OrderAddressType', $orderAddress);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($orderAddress);
            $em->flush();

            return $this->redirectToRoute('bunker_orderaddress_edit', array('id' => $orderAddress->getId()));
        }

        return $this->render('orderaddress/edit.html.twig', array(
            'orderAddress' => $orderAddress,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a OrderAddress entity.
     *
     */
    public function deleteAction(Request $request, OrderAddress $orderAddress)
    {
        $form = $this->createDeleteForm($orderAddress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($orderAddress);
            $em->flush();
        }

        return $this->redirectToRoute('bunker_orderaddress_index');
    }

    /**
     * Creates a form to delete a OrderAddress entity.
     *
     * @param OrderAddress $orderAddress The OrderAddress entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OrderAddress $orderAddress)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bunker_orderaddress_delete', array('id' => $orderAddress->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
