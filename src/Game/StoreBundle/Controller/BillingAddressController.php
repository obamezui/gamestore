<?php

namespace Game\STOREBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Game\STOREBundle\Entity\BillingAddress;
use Game\STOREBundle\Form\BillingAddressType;

/**
 * BillingAddress controller.
 *
 */
class BillingAddressController extends Controller
{
    /**
     * Lists all BillingAddress entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $billingAddresses = $em->getRepository('STOREBundle:BillingAddress')->findAll();

        return $this->render('billingaddress/index.html.twig', array(
            'billingAddresses' => $billingAddresses,
        ));
    }

    /**
     * Creates a new BillingAddress entity.
     *
     */
    public function newAction(Request $request)
    {
        $billingAddress = new BillingAddress();
        $form = $this->createForm('Game\STOREBundle\Form\BillingAddressType', $billingAddress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($billingAddress);
            $em->flush();

            return $this->redirectToRoute('bunker_billingaddress_show', array('id' => $billingAddress->getId()));
        }

        return $this->render('billingaddress/new.html.twig', array(
            'billingAddress' => $billingAddress,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a BillingAddress entity.
     *
     */
    public function showAction(BillingAddress $billingAddress)
    {
        $deleteForm = $this->createDeleteForm($billingAddress);

        return $this->render('billingaddress/show.html.twig', array(
            'billingAddress' => $billingAddress,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing BillingAddress entity.
     *
     */
    public function editAction(Request $request, BillingAddress $billingAddress)
    {
        $deleteForm = $this->createDeleteForm($billingAddress);
        $editForm = $this->createForm('Game\STOREBundle\Form\BillingAddressType', $billingAddress);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($billingAddress);
            $em->flush();

            return $this->redirectToRoute('bunker_billingaddress_edit', array('id' => $billingAddress->getId()));
        }

        return $this->render('billingaddress/edit.html.twig', array(
            'billingAddress' => $billingAddress,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a BillingAddress entity.
     *
     */
    public function deleteAction(Request $request, BillingAddress $billingAddress)
    {
        $form = $this->createDeleteForm($billingAddress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($billingAddress);
            $em->flush();
        }

        return $this->redirectToRoute('bunker_billingaddress_index');
    }

    /**
     * Creates a form to delete a BillingAddress entity.
     *
     * @param BillingAddress $billingAddress The BillingAddress entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(BillingAddress $billingAddress)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bunker_billingaddress_delete', array('id' => $billingAddress->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
