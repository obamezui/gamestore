<?php

namespace Game\STOREBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Orders
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="Game\STOREBundle\Repository\OrdersRepository")
 */
class Orders
{
    /**
* @var int
*
* @ORM\Column(name="id", type="integer")
* @ORM\Id
* @ORM\GeneratedValue(strategy="AUTO")
*/
private $id;

/**
 * @var string
 * @ORM\Column(name="order_date", type="string", length=50)
 */
private $order_date;

 /**
 * @ORM\ManyToOne(targetEntity="Customer")
 *@ORM\JoinColumn(name="customer_id",referencedColumnName="id")
 */
private $customer;

/**
* @ORM\OneToOne(targetEntity="Cart")
* @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
*/
private $product;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderDate
     *
     * @param string $orderDate
     *
     * @return Orders
     */
    public function setOrderDate($orderDate)
    {
        $this->order_date = $orderDate;

        return $this;
    }

    /**
     * Get orderDate
     *
     * @return string
     */
    public function getOrderDate()
    {
        return $this->order_date;
    }

    /**
     * Set customer
     *
     * @param \Game\STOREBundle\Entity\Customer $customer
     *
     * @return Orders
     */
    public function setCustomer(\Game\STOREBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Game\STOREBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set product
     *
     * @param \Game\STOREBundle\Entity\Cart $product
     *
     * @return Orders
     */
    public function setProduct(\Game\STOREBundle\Entity\Cart $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Game\STOREBundle\Entity\Cart
     */
    public function getProduct()
    {
        return $this->product;
    }
}
