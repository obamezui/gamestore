<?php

namespace Game\STOREBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * OrderAddress
 *
 * @ORM\Table(name="order_address")
 * @ORM\Entity(repositoryClass="Game\STOREBundle\Repository\OrderAddressRepository")
 */
class OrderAddress
{
     /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="street", type="string", length=50)
     */
    

    private $street;

     /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="city", type="string", length=50)
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(name="addr_complement", type="string", length=50)
     */
    private $addr_complement;

    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="zipcode", type="string", length=50)
     */
    private $zipcode;

     /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="country", type="string", length=50)
     */
    private $country;

     /**
     * @ORM\OneToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return OrderAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return OrderAddress
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set addrComplement
     *
     * @param string $addrComplement
     *
     * @return OrderAddress
     */
    public function setAddrComplement($addrComplement)
    {
        $this->addr_complement = $addrComplement;

        return $this;
    }

    /**
     * Get addrComplement
     *
     * @return string
     */
    public function getAddrComplement()
    {
        return $this->addr_complement;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return OrderAddress
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return OrderAddress
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set customer
     *
     * @param \Game\STOREBundle\Entity\Customer $customer
     *
     * @return OrderAddress
     */
    public function setCustomer(\Game\STOREBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Game\STOREBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
