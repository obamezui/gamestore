<?php

namespace Game\STOREBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Customer
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity(repositoryClass="Game\STOREBundle\Repository\CustomerRepository")
 */
class Customer
{
    /**
 *@ORM\Column(type="guid")
 *@ORM\Id
 * @ORM\GeneratedValue(strategy="UUID")
 */
protected $id;

/**
 * @var string
 * *@Assert\NotBlank()
 * @ORM\Column(name="name", type="string", length=50)
 */
private $name;

/**
 * @var string
 * *@Assert\NotBlank()
 * @ORM\Column(name="first_name", type="string", length=50)
 */
private $first_name;


/**
 *@ORM\OneToMany(targetEntity="Orders",mappedBy="customer")
 */
private $orders;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Customer
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Add order
     *
     * @param \Game\STOREBundle\Entity\Order $order
     *
     * @return Customer
     */
    public function addOrder(\Game\STOREBundle\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \Game\STOREBundle\Entity\Order $order
     */
    public function removeOrder(\Game\STOREBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }
}
