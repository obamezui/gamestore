<?php

namespace Game\STOREBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\MappedSuperclass
 * Product
 */
class Product
{
 /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
/**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="description", type="string", length=100)
     */
    protected $description;

/**
     * @var string
     * @ORM\Column(name="name", type="string", length=50)
     */
    protected $name;

          /**
     * @var string $image
     * @ORM\Column(name="image", type="string", length=255)
     */
    protected $image;

    /**
     * @Assert\File(maxSize = "2048k", mimeTypesMessage = "veuillez choisir une image valide")
     */
    protected $picture;

/**
     * @var string
     *  @Assert\Range(
     *      min = 1,
     *      minMessage = "le prix d'un article ne peut être inférieur à {{limit}}"
     * )
     * @ORM\Column(name="price", type="decimal",precision=8, scale=2)
     */
    protected $price;

    /**
*@ORM\OneToOne(targetEntity="CartItem", mappedBy="product")
*/
protected $cartitem;
    
}

/**
 * Game
 * 
 * @ORM\Table(name="Game")
 * @ORM\Entity(repositoryClass="Game\STOREBundle\Repository\GameRepository")
 */
class Game extends Product
{
    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="game_name", type="string", length=50)
     */
    private $game_name;

    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="number_player", type="string", length=10)
     */
    private $number_player;

    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="pg_age", type="string", length=10)
     */
    private $pg_age;
}

/**
 * Console
 * 
 * @ORM\Table(name="Console")
 * @ORM\Entity(repositoryClass="Game\STOREBundle\Repository\ConsoleRepository")
 */
class Console extends Product
{
    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="console_name", type="string", length=50)
     */
    private $console_name;

    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="console_brand", type="string", length=10)
     */
    private $console_brand;

     /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="console_memory", type="string", length=10)
     */
    private $console_memory;

}

/**
 * Accessorie
 * 
 * @ORM\Table(name="Accessorie")
 * @ORM\Entity(repositoryClass="Game\STOREBundle\Repository\AccessorieRepository")
 */
class Accessorie extends Product
{
    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="accessory_name", type="string", length=50)
     */
    private $accessory_name;

    /**
     * @var string
     * *@Assert\NotBlank()
     * @ORM\Column(name="accessory_description", type="string", length=100)
     */
    private $accessory_description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
