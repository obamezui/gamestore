<?php

namespace Game\STOREBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CartItem
 *
 * @ORM\Table(name="cart_item")
 * @ORM\Entity(repositoryClass="Game\STOREBundle\Repository\CartItemRepository")
 */
class CartItem
{
    /**
* @var int
*
* @ORM\Column(name="id", type="integer")
* @ORM\Id
* @ORM\GeneratedValue(strategy="AUTO")
*/
private $id;

/**
*
* @ORM\Column(name="quantity", type="integer")
*/
private $quantity;

/**
 * @ORM\ManyToOne(targetEntity="Cart")
 * @ORM\JoinColumn(name="cart_id",referencedColumnName="id")
 */
private $cart;

/**
* @ORM\OneToOne(targetEntity="Product",inversedBy="cartitem")
* @ORM\JoinColumn(name="product_id", referencedColumnName="id")
*/
private $product;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return CartItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set cart
     *
     * @param \Game\STOREBundle\Entity\Cart $cart
     *
     * @return CartItem
     */
    public function setCart(\Game\STOREBundle\Entity\Cart $cart = null)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return \Game\STOREBundle\Entity\Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Set product
     *
     * @param \Game\STOREBundle\Entity\Product $product
     *
     * @return CartItem
     */
    public function setProduct(\Game\STOREBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Game\STOREBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
