<?php

namespace Game\StoreBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Customer
 * @ORM\Table(name="Customer")
 * @ORM\Entity
 */
<?php 

namespace Game\StoreBundle\Entity;

/**
 *Order
 * 
 *@ORM\Entity
 *@ORM\Table(name="order")
 */
class Order
{

/**
* @var int
*
* @ORM\Column(name="id", type="integer")
* @ORM\Id
* @ORM\GeneratedValue(strategy="AUTO")
*/
private $id;

/**
 * @var string
 * @ORM\Column(name="order_date", type="string", length=50)
 */
private $order_date;

 /**
 * @ORM\ManyToOne(targetEntity="Customer")
 *@ORM\JoinColumn(name="customer_id",referencedColumnName="id")
 */
private $customer;

/**
* @OneToOne(targetEntity="Cart")
* @JoinColumn(name="cart_id", referencedColumnName="id")
*/
private $product;



}
