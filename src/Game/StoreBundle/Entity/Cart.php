<?php

namespace Game\STOREBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * Cart
 *
 * @ORM\Table(name="cart")
 * @ORM\Entity(repositoryClass="Game\STOREBundle\Repository\CartRepository")
 * @ExclusionPolicy("all")
 */
class Cart
{
   /**
* @var int
*
*@ORM\Column(name="id", type="integer")
*@ORM\Id
*@ORM\GeneratedValue(strategy="AUTO")
 * @Expose
*/
private $id;

/**
*@ORM\OneToMany(targetEntity="CartItem",mappedBy="cart")
 * @Expose
*/
private $cartitems;

/**
*@ORM\OneToOne(targetEntity="Customer")
*@ORM\JoinColumn(name="customer_id", referencedColumnName="id")
 * @Expose
*/
private $customer;

 public function __construct()
    {
        $this->cartitems = new ArrayCollection();
    }
 
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add cartitem
     *
     * @param \Game\STOREBundle\Entity\CartItem $cartitem
     *
     * @return Cart
     */
    public function addCartitem(\Game\STOREBundle\Entity\CartItem $cartitem)
    {
        $this->cartitems[] = $cartitem;

        return $this;
    }

    /**
     * Remove cartitem
     *
     * @param \Game\STOREBundle\Entity\CartItem $cartitem
     */
    public function removeCartitem(\Game\STOREBundle\Entity\CartItem $cartitem)
    {
        $this->cartitems->removeElement($cartitem);
    }

    /**
     * Get cartitems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartitems()
    {
        return $this->cartitems;
    }

    /**
     * Set customer
     *
     * @param \Game\STOREBundle\Entity\Customer $customer
     *
     * @return Cart
     */
    public function setCustomer(\Game\STOREBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Game\STOREBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
